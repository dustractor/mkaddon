" Tapu Project

" files of editing
call argloco#TabArgs([
            \   ["mk","Makefile"],
            \   ["proj","proj.vim"],
            \   ["tmux","tmux.session.sh"],
            \   ["mkaddon","__init__.py"],
            \   ])

" commands of doing
com MakeMaker silent! call system("sh -c 'urxvt -bl -name tiny -geometry 80x72+0+0 -cd /home/user/bpy/scripts/addons/tapu/ -e ./tmux.session.sh Tapu Maker' &")
com MakerMake silent! call system('tmux send-keys -t Tapu:Maker.0 make Enter')
com TmuxDeath silent! call system('tmux kill-server')

" buttons of pressing
nmap <f11> :MakeMaker<CR>
nmap [23$ :TmuxDeath<CR>
nmap <f12> :MakerMake<CR>

