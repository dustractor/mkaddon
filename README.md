# README #


### mkaddon ###

* "mk" as in make -- "addon" as in python script for Blender.

* Version 0.00.001

### How do I get set up? ###

* Summary of set up

Install of blender addon the normal way ( download the zip -> go to user prefs addons -> install from file -> choose the zip )

* Configuration

None.  Yet.  Future is plan to make directory for auto the thing get on the list where you can use it.

Not even configurable in the least, as the whole thing is hooked up off C.window_manager of which properties thereof are not saved.  Once the thing is -- right -- then that would be version 1.0, but *right* as in saveable with file is my 0.1.xxx target and I'm only on 0.00.001 ...

* Dependencies

There are no dependencies other than blender itself.  The python modules used are standard library.

### Contribution guidelines ###

There are no guidelines.  Yee Haw!  I have an email and that's it.

* Writing tests

Do not do that, please.

* Code review

I won't review yours if you don't review mine, or maybe vice versa...



### Who do I talk to? ###

* Repo owner or admin  dustractor@gmail.com
* Other community or team contact you@maybe.dotcom