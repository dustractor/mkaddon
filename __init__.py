bl_info = {
    "name": "mkaddon",
    "author": "dustractor@gmail.com",
    "version": (0, 1),
    "blender": (2, 7, 2),
    "location": "Text Editor side pane -> mkaddon panel",
    "description": "Makes Minimal Boilerplate (Operators, Panels ... )",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Text Editor"}

import bpy
import uuid
import string

t_info = """

bl_info = {
    "name": "${addon_name}",
    "author": "${author}",
    "version": (0, 1),
    "blender": (2, 7, 2),
    "location": "${location}",
    "description": "${description}",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "${category}"}

"""
t_import = """

import bpy

"""

t_opexec = """

class ${modname}_OT_${opname}(bpy.types.Operator):
    bl_idname = "$modname.$opname"
    bl_label = "$modname:$opname"
    def execute(self,context):
        print("self:",self)
        return {"FINISHED"}

"""

t_panel = """

class ${modname}_PT_${panel_name}(bpy.types.Panel):
    bl_space_type = "$bl_space_type"
    bl_region_type = "$bl_region_type"
    bl_label = "$bl_label"
    bl_category = "$bl_category"
    def draw(self,context):
        layout = self.layout
        layout.label("Hello World!")

"""

t_registration = """

def register():
    bpy.utils.register_module(__name__)

def unregister():
    bpy.utils.unregister_module(__name__)

"""

ttype_map = {
        "INFO":     t_info,
        "IMPORT":   t_import,
        "OPEXEC":   t_opexec,
        "PANEL":    t_panel,
        "REGISTER": t_registration
        }

ttype_enum = (
        ("CUSTOM",   "Custom","Custom text"),
        ("INFO",     "Addon Info","bl_info section that must be present"),
        ("IMPORT",   "Import","the old import bpy line which is usually good to have"),
        ("OPEXEC",   "Operator (exec)","Operator template [with execute()]"),
        ("PANEL",    "Panel","A panel"),
        ("REGISTER", "Registration","Ye olde register and unregister functions"),
        )

_toggled_panels = bpy.types.TEXT_PT_find,bpy.types.TEXT_PT_properties
_toggle_functions = bpy.utils.register_class,bpy.utils.unregister_class

def toggle_full(self,context):
    list(
            map(
                _toggle_functions[self.only],
                _toggled_panels
                )
            )

def addon_update(self,context):
    mkp = context.window_manager.mkaddon
    txt = bpy.data.texts.get(mkp.name,None)
    if not txt:
        txt = bpy.data.texts.new(mkp.name)
    context.area.spaces[0].text = txt
    tvars = set()
    for section in mkp.sections:
        for tvar in section._tvars:
            tvars.add(tvar)
    htvars = set([tv.name for tv in mkp.template_vars])
    addtvars = tvars - htvars
    dxvars = htvars - tvars
    for atv in addtvars:
        xtv = mkp.template_vars.add()
        xtv.name = atv
    dxi_ls = []
    for tv in dxvars:
        for n,qtv in enumerate(mkp.template_vars):
            if qtv.name == tv:
                dxi_ls.append(n)
    for dxv in reversed(sorted(dxi_ls)):
        mkp.template_vars.remove(dxv)
    mkp.tv_L = len(mkp.template_vars)
    maptv = {_.name:_.value for _ in mkp.template_vars}
    ts = string.Template(mkp.computed).safe_substitute(maptv)
    txt.from_string(ts)
    bpy.ops.text.jump()


class MKADDON_OT_add_section(bpy.types.Operator):
    bl_label = "mkaddon:add section"
    bl_idname = "mkaddon.add_section"
    bl_options = {"INTERNAL"}
    def execute(self,context):
        mkp = context.window_manager.mkaddon
        mkp.add_section()
        return {"FINISHED"}


class MKADDON_OT_del_section(bpy.types.Operator):
    bl_label = "mkaddon:delete section"
    bl_idname = "mkaddon.del_section"
    bl_options = {"INTERNAL"}
    index = bpy.props.IntProperty(min=-1,default=-1)
    def execute(self,context):
        mkp = context.window_manager.mkaddon
        if self.index > -1:
            mkp.sections.remove(self.index)
            if len(mkp.sections):
                mkp.sections_i = max(0,mkp.sections_i-1)
            else:
                mkp.sections_i = -1
            context.area.tag_redraw()
        return {"FINISHED"}


class MKADDON_OT_move_section(bpy.types.Operator):
    bl_label = "mkaddon:move section"
    bl_idname = "mkaddon.move_section"
    bl_options = {"INTERNAL"}
    direction = bpy.props.EnumProperty(
            items=[(_,)*3 for _ in ("UP","DOWN")],
            default="UP")
    def execute(self,context):
        mkp = context.window_manager.mkaddon
        idx = mkp.sections_i
        nidx = idx + [1,-1][self.direction=="UP"]
        mkp.sections.move(idx,nidx)
        mkp.sections_i = min(len(mkp.sections),max(0,nidx))
        return {"FINISHED"}


class MKADDON_UL_sections(bpy.types.UIList):
    def draw_item(self,context,layout,data,item,icon,actvdata,actvprop):
        row = layout.row(align=True)
        row.label(item.name)
        row.prop(item,"ttype",text="")


class AddonSection(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(default="section",update=addon_update)
    custom_text = bpy.props.StringProperty(default="#",update=addon_update)
    ttype = bpy.props.EnumProperty(
            items=ttype_enum,default="CUSTOM",
            update=addon_update)
    @property
    def _tvars(self):
        s = set()
        for t in string.Template.pattern.findall(self._template):
            for ti in t:
                s.add(ti)
        return list(filter(None,s))
    @property
    def _template(self):
        return (self.custom_text
                if self.ttype == "CUSTOM" else
                ttype_map.get(self.ttype,"?"))
    @property
    def computed(self):
        return (self.custom_text
                if self.ttype == "CUSTOM" else
                self._template)
    def display(self,context,layout):
        layout.label(self.name)
        layout.prop(self,"name")
        layout.separator()
        layout.prop(self,"ttype")
        if self.ttype == "CUSTOM":
            layout.prop(self,"custom_text")
        else:
            pass


class TemplateVar(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty()
    value = bpy.props.StringProperty(update=addon_update)


class MkAddon(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(
            default="mk_"+uuid.uuid4().hex[:5]+".py",
            update=addon_update)
    sections = bpy.props.CollectionProperty(type=AddonSection)
    sections_i = bpy.props.IntProperty(min=-1,default=-1,update=addon_update)
    only = bpy.props.BoolProperty(update=toggle_full)
    compendium = bpy.props.StringProperty()
    template_vars = bpy.props.CollectionProperty(type=TemplateVar)
    tv_L = bpy.props.IntProperty()
    def add_section(self):
        section = self.sections.add()
        self.sections_i += 1
    @property
    def computed(self):
        t = list()
        for section in self.sections:
            computed = section.computed
            t.append(computed)
        return "\n".join(t)
    def display(self,context,layout):
        layout.prop(self,"name")
        layout.label(self.name)
        layout.label("Sections")
        row = layout.row(align=True)
        col1 = row.column(align=True)
        col1.operator("mkaddon.add_section",text="",icon="ZOOMIN")
        col2 = row.column(align=True)
        col2.enabled = self.sections_i >= 0
        op = col2.operator("mkaddon.del_section",text="",icon="X")
        op.index = self.sections_i
        layout.template_list(
                "MKADDON_UL_sections",
                "",
                self,
                "sections",
                self,
                "sections_i")
        active_section = self.sections[self.sections_i] if (
                -1 < self.sections_i < len(self.sections)) else None
        if active_section:
            row.separator()
            col3 = row.column(align=True)
            col3.enabled = self.sections_i > 0
            col4 = row.column(align=True)
            col4.enabled = (
                    (self.sections_i > -1) and
                    (self.sections_i < (len(self.sections) - 1)))
            op = col3.operator(
                    "mkaddon.move_section",
                    text="",
                    icon="TRIA_UP")
            op.direction = "UP"
            op = col4.operator(
                    "mkaddon.move_section",
                    text="",
                    icon="TRIA_DOWN")
            op.direction = "DOWN"
            row.separator()
            row.prop(active_section,"name")
            box = layout.box()
            active_section.display(context,box)
        if self.tv_L:
            box = layout.box()
            for tv in self.template_vars:
                box.prop(tv,"value",text=tv.name)


class MKADDON_PT_mkaddon_panel(bpy.types.Panel):
    bl_space_type = "TEXT_EDITOR"
    bl_region_type = "UI"
    bl_label = "mkaddon"
    def draw_header(self,context):
        self.layout.prop(
                context.window_manager.mkaddon,
                "only",
                toggle=True,
                text="",
                icon="FULLSCREEN")
    def draw(self,context):
        context.window_manager.mkaddon.display(context,self.layout)


def register():
    bpy.utils.register_module(__name__)
    bpy.types.WindowManager.mkaddon = bpy.props.PointerProperty(type=MkAddon)

def unregister():
    del bpy.types.WindowManager.mkaddon
    bpy.utils.unregister_module(__name__)

